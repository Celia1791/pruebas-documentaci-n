## Descripción 

Es la descripción de la funcionalidad del SW , el contexto en donde se desarrollo y los problemas de desarrollo que se ayudo a resolver.

## Guía de usuario
Este punto es un paso a paso, que en especifico va dirigido al usuario final sobre como iniciar con el uso de la herramienta digital. Si esta información es demasiado extensa, se recomienda que se tenga un documento diferente, pero el cual debe de estar mencionado en este ReadMe.

## Guía de instalación 
En este apartado debemos de tomar en cuenta la serie de instrucciones de instalación y configurar la herramienta, esta sección va dirigida a desarrolladores.
 Durante este apartado se debe de tomar en cuenta tres puntos.
 
 - Los requisitos del sistema operativo para la compilación el SW (versiones especificas de gestión de paquetes y dependencias, SDKS y compiladores)
 - Las dependencias propias  del proyecto, tanto externas como internas (orden de compilación de sub-modulos),  configuración de ubicación de librerías dinámicas, etc).
 - Pasos específicos para la compilación del código fuente, así como la ejecución de test unitarios en caso de que se requiera en el proyecto
 
 ##  Autores
 Se da el crédito a los colaboradores de la herramienta.

## Apartados opcionales:
- Licencia para el código  
- Licencia para la documentación de la herramienta.
- Como contribuir a la herramienta
- Código de conducta
-  Insignias (badges)
- Versión
- Reconocimientos 


# Ejemplo Readme

## Bis Mex Parts
La plataforma de Bis Mex Parts nos ayuda con la gestión de materiales dentro del área de producción para cummins.

## Como comenzar a utilizar la aplicación 
Para poder entender como funciona nuestra herramienta, tenemos que dirigirnos a el manual de usuario, el cual se encuentra en el siguiente enlace [enter link description here](https://gitlab.com/Celia1791/pruebas-documentaci-n/blob/master/Gu%C3%ADa%20de%20usuario.md)


## Requisitos del sistema operativo
para poder ingresar al código desde nuestro equipo, requerimos visual studio code, y permisos de administrador dentro de Sharepoint. 

Ahora bien dentro de la plataforma, necesitamos utilizar angular js 9.5.
para exportar nuestro código la primer vez debemos de acceder a través de internet explorer o bien configurar nuestro host para poder ingresar desde el explorador de nuestro equipo.

##  Orden de compilación de módulos

Los módulos deben de compilarse de la siguiente para el proceso de pruebas integrales.
1. parts
2. Formulario parts
3. Vistas
4. Flujos de Nintex

## Licencia para el código
Dentro del desarrollo de esta proyecto necesitamos tener 
* Licencias de Sharepoint, ya se online, onpremise o bien hibrido.
* Licencias de Nintex 

## Autores
* Carlos Mediana (Desarrollador JS)
* Francisco Arias  (Desarrollador JS)
* Scarlet Rodríguez (Análista SP y QA)

    

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTMxMzIyNDA0M119
-->